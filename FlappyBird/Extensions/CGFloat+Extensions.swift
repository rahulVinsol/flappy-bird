//
//  CGFloat+Extensions.swift
//  FlappyBird
//
//  Created by Rahul Rawat on 05/07/21.
//

import Foundation
import CoreGraphics

let π = CGFloat(Double.pi)

public extension CGFloat {
    func degreesToRadians() -> CGFloat {
        return π * self / 180.0
    }
    
    func radiansToDegrees() -> CGFloat {
        return self * 180.0 / π
    }
    
    func clamped(v1: CGFloat, _ v2: CGFloat) -> CGFloat {
        let min = v1 < v2 ? v1 : v2
        let max = v1 > v2 ? v1 : v2
        return self < min ? min : (self > max ? max : self)
    }
    
    mutating func clamp(v1: CGFloat, _ v2: CGFloat) {
        self = clamped(v1: v1, v2)
    }
    
    func sign() -> CGFloat {
        return (self >= 0.0) ? 1.0 : -1.0
    }

    static func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFF)
    }
    
    static func random(min: CGFloat, max: CGFloat) -> CGFloat {
        assert(min < max)
        return CGFloat.random() * (max - min) + min
    }
    
    static func randomSign() -> CGFloat {
        return (arc4random_uniform(2) == 0) ? 1.0 : -1.0
    }
}

public func shortestAngleBetween(angle1: CGFloat, angle2: CGFloat) -> CGFloat {
    let twoπ = π * 2.0
    
    var angle = (angle2 - angle1) .truncatingRemainder(dividingBy: twoπ)
    if (angle >= π) {
        angle = angle - twoπ
    }
    if (angle <= -π) {
        angle = angle + twoπ
    }
    return angle
}
