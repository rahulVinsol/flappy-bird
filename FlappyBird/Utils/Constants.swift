//
//  Constants.swift
//  FlappyBird
//
//  Created by Rahul Rawat on 05/07/21.
//

import Foundation

struct Constants {
    static var shared: Constants = Constants()
    
    private init() {
        self.choosenTheme = dayThemeAssetKeys
    }
    
    let startTutorialAssetKey = "message"
    let baseAssetKey = "base"
    let gameOverAssetKey = "gameover"
    
    let hurdlePairName = "Hurdle Pair"
    let fontFamilyName = "04b_19"
    
    let dayThemeAssetKeys = ThemeAssetKeys(birdDownFlap: "yellowbird-downflap", birdMidFlap: "yellowbird-midflap", birdUpFlap: "yellowbird-upflap", background: "background-day", pipePart: "pipe-green-part", pipe: "pipe-green")
    
    let nightThemeAssetKeys = ThemeAssetKeys(birdDownFlap: "bluebird-downflap", birdMidFlap: "bluebird-midflap", birdUpFlap: "bluebird-upflap", background: "background-night", pipePart: "pipe-red-part", pipe: "pipe-red")

    var choosenTheme: ThemeAssetKeys!
}

struct ThemeAssetKeys {
    let birdDownFlap: String
    let birdMidFlap: String
    let birdUpFlap: String
    let background: String
    let pipePart: String
    let pipe: String
    
    fileprivate init(birdDownFlap: String, birdMidFlap: String, birdUpFlap: String, background: String, pipePart: String, pipe: String) {
        self.birdDownFlap = birdDownFlap
        self.birdMidFlap = birdMidFlap
        self.birdUpFlap = birdUpFlap
        self.background = background
        self.pipePart = pipePart
        self.pipe = pipe
    }
}
