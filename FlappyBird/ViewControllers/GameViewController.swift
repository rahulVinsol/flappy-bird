//
//  GameViewController.swift
//  FlappyBird
//
//  Created by Rahul Rawat on 03/07/21.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Constants.shared.choosenTheme = self.traitCollection.userInterfaceStyle == .dark ? Constants.shared.nightThemeAssetKeys : Constants.shared.dayThemeAssetKeys
        
        let scene = GameScene(size: view.frame.size)
        let skView = self.view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .aspectFill
        skView.presentScene(scene)
    }
    
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool { true }
}
