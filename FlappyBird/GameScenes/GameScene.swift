//
//  GameScene.swift
//  FlappyBird
//
//  Created by Rahul Rawat on 03/07/21.
//

import SpriteKit
import GameplayKit

struct NodeZScale {
    static let foreground: CGFloat = 5
    static let bird: CGFloat = 4
    static let startTutorial: CGFloat = 3
    static let base: CGFloat = 2
    static let hurdle: CGFloat = 1
    static let background: CGFloat = 0
}

struct PhysicsCategory {
    static let none: UInt32 = 0x1 << 0
    static let bird: UInt32 = 0x1 << 1
    static let base: UInt32 = 0x1 << 2
    static let hurdle: UInt32 = 0x1 << 3
    static let background: UInt32 = 0x1 << 4
    static let scoreNode: UInt32 = 0x1 << 5
}

class GameScene: SKScene {
    private let fixedDelta: CFTimeInterval = 1.0 / 60.0
    
    private var themeAssetKeys: ThemeAssetKeys!
    
    private var base: SKSpriteNode!
    private var bird: SKSpriteNode!
    private var scoreLabel: SKLabelNode!
    private var restartButton: SKSpriteNode!
    private var startTutorial: SKSpriteNode!
    
    private var moveAndRemove: SKAction!
    
    private var hasGameStarted: Bool = false
    private var score: Int = 0
    private var startTime: Double = 0
    private var sinceTouch : CFTimeInterval = 0
    
    private var hasDied: Bool = false
    
    override func didMove(to view: SKView) {
        themeAssetKeys = Constants.shared.choosenTheme
        
        restartScene()
        
        self.physicsWorld.contactDelegate = self
    }
    
    private func restartScene() {
        hasGameStarted = false
        hasDied = false
        score = 0
        startTime = 0
        
        self.removeAllChildren()
        
        self.addChild(createBackground())
        self.addChild(createBase())
        self.addChild(createBird())
        self.addChild(createStartTutorial())
    }
    
    private func createStartTutorial() -> SKSpriteNode {
        startTutorial = SKSpriteNode(imageNamed: Constants.shared.startTutorialAssetKey)
        
        startTutorial.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2 + 48)
        startTutorial.zPosition = NodeZScale.startTutorial
        
        return startTutorial
    }
    
    private func createBackground() -> SKSpriteNode {
        let background = SKSpriteNode(imageNamed: themeAssetKeys.background)
        background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        background.zPosition = NodeZScale.background
        background.xScale = frame.size.width / background.size.width
        background.yScale = frame.size.height / background.size.height
        
        let physicsBody = SKPhysicsBody(rectangleOf: background.size)
        physicsBody.categoryBitMask = PhysicsCategory.background
        physicsBody.collisionBitMask = PhysicsCategory.none
        physicsBody.contactTestBitMask = PhysicsCategory.none
        physicsBody.affectedByGravity = false
        physicsBody.isDynamic = false
        
        background.physicsBody = physicsBody
        
        return background
    }
    
    private func createBase() -> SKSpriteNode {
        base = SKSpriteNode(imageNamed: Constants.shared.baseAssetKey)
        //multiplying 1.2 to make base slightly bigger in width to prevent when bird crashes on a hurdle it rolls of the base's ends like it's a cliff
        base.xScale = frame.size.width / base.size.width * 1.2
        base.position = CGPoint(x: base.size.width / 2, y: base.size.height / 2)
        base.zPosition = NodeZScale.base
        
        let physicsBody = SKPhysicsBody(rectangleOf: base.size)
        physicsBody.categoryBitMask = PhysicsCategory.base
        physicsBody.collisionBitMask = PhysicsCategory.bird
        physicsBody.contactTestBitMask = PhysicsCategory.bird
        physicsBody.affectedByGravity = false
        physicsBody.isDynamic = false
        
        base.physicsBody = physicsBody
        
        return base
    }
    
    private func randomGap(min: CGFloat, max: CGFloat) -> CGFloat {
        return CGFloat.random(in: min...max)
    }
    
    private func createHurdles() -> SKNode {
        let hurdlePair = SKNode()
        
        let topHurdle = SKSpriteNode(imageNamed: themeAssetKeys.pipe)
        let bottomHurdle = SKSpriteNode(imageNamed: themeAssetKeys.pipe)
        
        let neededGapHeight = CGFloat(125)
        
        // MARK: - Calculating the gap in between top and bottom pipes
        let gapHeight = frame.size.height - neededGapHeight - topHurdle.size.height - bottomHurdle.size.height
        
        // MARK: - Creating top and bottom hurdle
        topHurdle.position = CGPoint(x: self.frame.width + topHurdle.size.width / 2, y: self.frame.height -  topHurdle.size.height / 2 - gapHeight / 2)
        topHurdle.zPosition = NodeZScale.hurdle
        topHurdle.zRotation = CGFloat.pi
        topHurdle.xScale = -1
        
        bottomHurdle.position = CGPoint(x: self.frame.width + bottomHurdle.size.width / 2, y: bottomHurdle.size.height / 2 + gapHeight / 2)
        bottomHurdle.zPosition = NodeZScale.hurdle
        bottomHurdle.zRotation = CGFloat.zero
        
        // MARK: - Calculating y offset to make hurdle pair's gap to occur at random height
        let minRandomValue = CGFloat(base.position.y - bottomHurdle.position.y + (base.size.height - bottomHurdle.size.height) / 2)
        let maxRandomValue = CGFloat(frame.height - topHurdle.position.y + topHurdle.size.height / 2)
        let yOffset = randomGap(min: minRandomValue, max: maxRandomValue)
        
        // MARK: - Giving physics body to top and bottom hurdle
        var physicsBody = SKPhysicsBody(rectangleOf: topHurdle.size)
        physicsBody.categoryBitMask = PhysicsCategory.hurdle
        physicsBody.collisionBitMask = PhysicsCategory.bird
        physicsBody.contactTestBitMask = PhysicsCategory.bird
        physicsBody.affectedByGravity = false
        physicsBody.isDynamic = false
        
        topHurdle.physicsBody = physicsBody
        
        physicsBody = SKPhysicsBody(rectangleOf: bottomHurdle.size)
        physicsBody.categoryBitMask = PhysicsCategory.hurdle
        physicsBody.collisionBitMask = PhysicsCategory.bird
        physicsBody.contactTestBitMask = PhysicsCategory.bird
        physicsBody.affectedByGravity = false
        physicsBody.isDynamic = false
        
        bottomHurdle.physicsBody = physicsBody
        
        // MARK: - Creating pipe part to add in place of gap that occurs due to yOffset
        let pipePart = SKSpriteNode(imageNamed: themeAssetKeys.pipePart)
        let pipePartHeightRequired: CGFloat
        pipePart.zPosition = NodeZScale.hurdle
        
        // MARK: - Creating gap pipe part to add in opposite of pipe part that will create a gap due to gapHeight costraint
        let gapPipePart = SKSpriteNode(imageNamed: themeAssetKeys.pipePart)
        gapPipePart.zPosition = NodeZScale.hurdle
        gapPipePart.yScale = gapHeight / pipePart.size.height
        
        // MARK: - Calculating pipePart's and gapPipePart's positions and heightsto prevent showing of a gap in pipe
        if yOffset < 0 {
            pipePartHeightRequired = frame.height - yOffset - (topHurdle.position.y + topHurdle.size.height / 2)
            pipePart.position = CGPoint(x: self.frame.width + topHurdle.size.width / 2, y: topHurdle.position.y + topHurdle.size.height / 2 + pipePartHeightRequired / 2)
            pipePart.zRotation = CGFloat.pi
            pipePart.xScale = -1
            
            gapPipePart.position = CGPoint(x: self.frame.width + bottomHurdle.size.width / 2, y: bottomHurdle.position.y - bottomHurdle.size.height / 2 - gapHeight / 2)
        } else {
            pipePartHeightRequired = bottomHurdle.position.y - bottomHurdle.size.height / 2 + yOffset
            pipePart.position = CGPoint(x: self.frame.width + bottomHurdle.size.width / 2, y: bottomHurdle.position.y - bottomHurdle.size.height / 2 - pipePartHeightRequired / 2)
            
            gapPipePart.position = CGPoint(x: self.frame.width + bottomHurdle.size.width / 2, y: topHurdle.position.y + topHurdle.size.height / 2 + gapHeight / 2)
            gapPipePart.zRotation = CGFloat.pi
            gapPipePart.xScale = -1
        }
        
        // MARK: - Giving pipe and gapPipe their physics body
        pipePart.yScale = pipePartHeightRequired / pipePart.size.height
        
        physicsBody = SKPhysicsBody(rectangleOf: pipePart.size)
        physicsBody.categoryBitMask = PhysicsCategory.hurdle
        physicsBody.collisionBitMask = PhysicsCategory.bird
        physicsBody.contactTestBitMask = PhysicsCategory.bird
        physicsBody.affectedByGravity = false
        physicsBody.isDynamic = false
        
        pipePart.physicsBody = physicsBody
        
        physicsBody = SKPhysicsBody(rectangleOf: gapPipePart.size)
        physicsBody.categoryBitMask = PhysicsCategory.hurdle
        physicsBody.collisionBitMask = PhysicsCategory.bird
        physicsBody.contactTestBitMask = PhysicsCategory.bird
        physicsBody.affectedByGravity = false
        physicsBody.isDynamic = false
        
        gapPipePart.physicsBody = physicsBody
        
        // MARK: - Adding score node that will be invisible and in midst of two pipes crossing this will result in score being added
        let scoreNodeHeight = topHurdle.position.y - topHurdle.size.height / 2 - (bottomHurdle.position.y + bottomHurdle.size.height / 2)
        let scoreNode = SKSpriteNode()
        scoreNode.size = CGSize(width: 1, height: scoreNodeHeight)
        scoreNode.position = CGPoint(x: self.frame.width + bottomHurdle.size.width / 2, y: bottomHurdle.position.y + bottomHurdle.size.height / 2 + scoreNode.size.height / 2)
        scoreNode.zPosition = NodeZScale.hurdle
        
        // MARK: - Giving physics body to score node
        physicsBody = SKPhysicsBody(rectangleOf: scoreNode.size)
        physicsBody.categoryBitMask = PhysicsCategory.scoreNode
        physicsBody.collisionBitMask = PhysicsCategory.none
        physicsBody.contactTestBitMask = PhysicsCategory.bird
        physicsBody.affectedByGravity = false
        physicsBody.isDynamic = false
        
        scoreNode.physicsBody = physicsBody
        
        // MARK: - Adding all the children to hurdlePair
        hurdlePair.addChild(topHurdle)
        hurdlePair.addChild(bottomHurdle)
        
        hurdlePair.addChild(pipePart)
        hurdlePair.addChild(gapPipePart)
        
        hurdlePair.addChild(scoreNode)
        
        hurdlePair.position.y = hurdlePair.position.y + yOffset
        
        hurdlePair.name = Constants.shared.hurdlePairName
        
        return hurdlePair
    }
    
    private func createBird() -> SKSpriteNode {
        bird = SKSpriteNode(imageNamed: themeAssetKeys.birdMidFlap)
        bird.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        bird.zPosition = NodeZScale.bird
        
        let physicsBody = SKPhysicsBody(circleOfRadius: bird.size.height / 2)
        physicsBody.categoryBitMask = PhysicsCategory.bird
        physicsBody.collisionBitMask = PhysicsCategory.base | PhysicsCategory.hurdle
        physicsBody.contactTestBitMask = PhysicsCategory.base | PhysicsCategory.hurdle
        physicsBody.affectedByGravity = false
        physicsBody.isDynamic = true
        
        bird.physicsBody = physicsBody
        
        return bird
    }
    
    private func createScoreLabel() -> SKNode {
        scoreLabel = SKLabelNode()
        scoreLabel.fontName = Constants.shared.fontFamilyName
        scoreLabel.fontSize = 48
        
        let safeAreaInsets = UIApplication.shared.windows.first?.safeAreaInsets
        scoreLabel.position = CGPoint(x: frame.width / 2, y: frame.height - (safeAreaInsets?.top ?? 0) - scoreLabel.fontSize - 10)
        scoreLabel.text = "\(score)"
        scoreLabel.zPosition = NodeZScale.foreground
        
        return scoreLabel
    }
    
    private func createRestartButton() -> SKSpriteNode {
        restartButton = SKSpriteNode(imageNamed: Constants.shared.gameOverAssetKey)
        restartButton.position = CGPoint(x: frame.width / 2, y: frame.height / 2)
        restartButton.zPosition = NodeZScale.foreground
        
        restartButton.setScale(0)
        
        restartButton.run(SKAction.scale(to: 1.0, duration: 0.4))
        
        return restartButton
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if hasDied {
            for touch in touches {
                let location = touch.location(in: self)
                if restartButton.contains(location) {
                    restartScene()
                    return
                }
            }
            
            return
        }
        
        if !hasGameStarted {
            hasGameStarted = true
            
            startTutorial.removeFromParent()
            self.addChild(createScoreLabel())
            
            let spawn = SKAction.run { [weak self] in
                guard let self = self else { return }
                
                let hurdlePair = self.createHurdles()
                hurdlePair.run(self.moveAndRemove)
                self.addChild(hurdlePair)
            }
            let delay = SKAction.wait(forDuration: 2)
            let spawnDelay = SKAction.sequence([spawn, delay])
            let spawnDelayForever = SKAction.repeatForever(spawnDelay)
            
            self.run(spawnDelayForever)
            
            let hurdlePair = self.createHurdles()
            
            let distance = CGFloat(frame.width + hurdlePair.frame.width)
            let moveHurdles = SKAction.moveBy(x: -distance - 50, y: 0, duration: 0.008 * Double(distance))
            let removeHurdles = SKAction.removeFromParent()
            moveAndRemove = SKAction.sequence([moveHurdles, removeHurdles])
            
            bird.physicsBody?.affectedByGravity = true
        }
        
        bird.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        bird.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 10))
        
        sinceTouch = 0
    }
    
    override func update(_ currentTime: TimeInterval) {
        if !hasDied && hasGameStarted {
            let velocityY = bird.physicsBody?.velocity.dy ?? 0
            
            if velocityY > 400 {
                bird.physicsBody?.velocity.dy = 400
            }
            
            if startTime == 0 {
                startTime = currentTime
            }
            
            let timeElapsed = currentTime - startTime
            
            switch Int(timeElapsed * 4) % 4 {
            case 0:
                bird.texture = SKTexture(imageNamed: themeAssetKeys.birdUpFlap)
            case 2:
                bird.texture = SKTexture(imageNamed: themeAssetKeys.birdDownFlap)
            default:
                bird.texture = SKTexture(imageNamed: themeAssetKeys.birdMidFlap)
            }
            
            let impulse: Double
            if sinceTouch > 0.2 {
                impulse = -200000 * fixedDelta
            } else {
                impulse = 200000 * fixedDelta
            }
            
            bird.physicsBody?.applyAngularImpulse(CGFloat(impulse))
            bird.zRotation.clamp(v1: CGFloat(-30).degreesToRadians(), CGFloat(30).degreesToRadians())
            bird.physicsBody?.angularVelocity.clamp(v1: -3, 3)
            
            sinceTouch += fixedDelta
        } else {
            bird.texture = SKTexture(imageNamed: themeAssetKeys.birdMidFlap)
        }
    }
}

extension GameScene: SKPhysicsContactDelegate {
    func didBegin(_ contact: SKPhysicsContact) {
        if hasDied {
            return
        }
        
        let firstBody = contact.bodyA
        let secondBody = contact.bodyB
        
        let categoryBitMasksOred = firstBody.categoryBitMask | secondBody.categoryBitMask
        
        if categoryBitMasksOred == PhysicsCategory.bird | PhysicsCategory.scoreNode {
            score += 1
            scoreLabel.text = "\(score)"
        } else if categoryBitMasksOred & PhysicsCategory.bird > 0 && categoryBitMasksOred & (PhysicsCategory.hurdle | PhysicsCategory.base) > 0 {
            hasDied = true
            
            bird.zRotation = CGFloat.zero
            
            self.removeAllActions()
            
            self.enumerateChildNodes(withName: Constants.shared.hurdlePairName) { skNode, _ in
                skNode.removeAllActions()
            }
            
            self.addChild(createRestartButton())
        }
    }
}
